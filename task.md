# Task
- Pelajari mulai dari soal 1 sampai dengan soal 7
- Pelajari maksud dari setiap built-in function yang digunakan dari tiap soal
- Tanyakan jika masih ada yang kurang dimengerti
- Usahakan kedepannya untuk menulis sebuah program kecil, masukkan beberap komentar untuk memberitahukan orang lain proses apa yang sedang atau ingin dilaksanakan